package Lab2.Serwlety;

import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import com.sun.istack.internal.logging.Logger;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet(name = "Wypisywanie", urlPatterns = {"/Wypisywanie"})
public class Wypisywanie extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws java.lang.ClassNotFoundException
     * @throws java.sql.SQLException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, ClassNotFoundException, SQLException {
        response.setContentType("text/html;charset=UTF-8");
        String inst=request.getParameter("instytucja");
        boolean imie = request.getParameter("czyimie") != null;
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head><meta><link rel='stylesheet' href='Style/css/components.css'>");
            out.println("<link rel='stylesheet' href='Style/css/icons.css'>");
            out.println("<link rel='stylesheet' href='Style/css/responsee.css'>");
            out.println("<title>Wypisywanie pracowników</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h2>Wypisywanie pracowników </h2>");
            out.println("<p>Lista pracowników instytucji: " +inst + "</p><br />");
            out.println(new Lab2.Trwalosc.LudzieQuery().getLudzieLista(imie));
            //new Lab2.Trwalosc.LudzieQuery().getLudzieLista(imie);
            //out.println(getDataFromDb());
            out.println("<a class=\'button rounded-full-btn reload-btn s-2 margin-bottom\' href=");
            out.println(request.getHeader("referer"));
            out.println("><i class='icon-sli-arrow-left'></i>Powrót</a>");
            out.println("</body>");
            out.println("</html>");
        }
    }
    private boolean polaczenie = false;
    private String data;
    public static boolean czyname = false;
    public static boolean czysurname = false;
    
    private String getDataFromDb() {
        try {
            polaczenie = Lab2.Dane.DbManager.Connect();
            if (polaczenie) {
                data = Lab2.Dane.DbManager.getData();
                polaczenie = Lab2.Dane.DbManager.Disconnect();
            }
            System.out.println("Polaczono.");
        } catch (ClassNotFoundException | SQLException ex) {
            //Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Niepolaczono.");
        }
        return data;
    }
  
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            java.util.logging.Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (ClassNotFoundException | SQLException ex) {
            java.util.logging.Logger.getLogger(Wypisywanie.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
