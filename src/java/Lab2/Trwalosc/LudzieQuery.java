/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lab2.Trwalosc;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;

public class LudzieQuery {
    private Session session = null;
    private List<Employees> ludzieList = null;
    private Query q = null;
    
    public String getLudzieLista(boolean OrderByImie) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            if (OrderByImie) {
                q = session.createQuery("from Employees order by name");
            } else {
                q = session.createQuery("from Employees");
            }
            ludzieList = (List<Employees>) q.list();
            session.close();
            tx.commit();
        } catch (HibernateException e) {
        }
        String data = getListaHTML(ludzieList);
        return data;
    }
    
    public void ExecuteCommand(String id, String imie, String nazwisko) {
        try {
            org.hibernate.Transaction tx = session.beginTransaction();
            q = session.createQuery("insert into Employees(" + id +", " + imie + ", " + nazwisko + ")");
            q.executeUpdate();
            session.close();
            tx.commit();
        } catch (HibernateException e) {
        }
    }
    
    private String getListaHTML(List<Employees> lista) {
        String wiersz;
        wiersz = ("<table><tr>");
        
        wiersz = wiersz.concat(
        "<td><b>ID</b></td>"
        + "<td><b>IMIE</b></td>"
        + "<td><b>NAZWISKO</b></td>");
        
        wiersz = wiersz.concat("</tr>");
        
        for (Employees ldz : lista) {
            wiersz = wiersz.concat("<tr>");
            wiersz = wiersz.concat("<td>" + ldz.getId() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getName() + "</td>");
            wiersz = wiersz.concat("<td>" + ldz.getSurname() + "</td>");
            wiersz = wiersz.concat("</tr>");
        }
        wiersz = wiersz.concat("</table>");
        return wiersz;
    }

    public LudzieQuery() {
        this.session = HibernateUtil.getSessionFactory().getCurrentSession();
    }
    
}
